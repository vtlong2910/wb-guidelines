# Coding Guidelines

## Character encoding / Line feed

### `*.js` `*.css` `*.scss` `*.isml` `*.properties`

- UTF-8 / LF

## JavaScript

### Code formatter

#### Prettier

- [Prettier](https://prettier.io/)
- Run Prettier for all JavaScript, css(scss) files which you updated
  - **It's required for code review**

##### Prettier for VS Code

- Install VS Code Prettier plug-in
  - [Prettier Formatter for Visual Studio Code](https://marketplace.visualstudio.com/items?itemName=esbenp.prettier-vscode)
- Add following settings to your `settings.json`
- Use following `.prettierrc`

Your settings.json

```javascript
// Set the default
"editor.formatOnSave": false,
// Enable per-language
"[javascript]": {
    "editor.formatOnSave": true
},
"[css]": {
    "editor.formatOnSave": true
},
"[scss]": {
    "editor.formatOnSave": true
},

"editor.tabSize": 2,
```

Your .prettierrc

- [https://bitbucket.org/wiredbeans/shiseido/src/master/.prettierrc](https://bitbucket.org/wiredbeans/shiseido/src/master/.prettierrc)

```javascript
{
  "endOfLine": "lf",
  "jsxSingleQuote": true,
  "singleQuote": true
}
```

- Allow to use single quote

[![Image from Gyazo](https://i.gyazo.com/058c0814e7fbf45766ba8f29248941fa.png)](https://gyazo.com/058c0814e7fbf45766ba8f29248941fa)

- Use Prettier default settings except "Single Quote" setting

##### Prettier error

- If an error occurs during formatting, check the following

###### Deprecated syntax

- Remove all `for each...in` from the code

###### Zero width space

- Remove all ["zero width space"](https://en.wikipedia.org/wiki/Zero-width_space) from the code

### Naming convention

#### File name

##### JavaScript file name

- Upper camel case
- `foobar/cartridge/scripts/helpers/FooBarHelper.js`

##### CSS/SCSS filename

- Upper camel case
- `foobar/cartridge/client/default/scss/FooBar.scss`

#### Function name

- Lower camel case

```javascript
function fooBarBaz() {
  //...
}
module.exports.fooBarBaz = fooBarBaz;
```

#### Variable name

- Lower camel case

```javascript
const fooBarBaz = true;
```


### Coding styles

#### Sample code

```javascript
 function isPremiumUser(user, userPoints) {
   // Stop to use old C-lang style variable declaration
   // var isPU, isPermanentUser, premiumUserRequirementsPoints;

   // Check parameters ASAP. if parameters are NG, return ASAP.
   if(!user || !userPoints) {
       return false;
   }

   // Avoid confusing expressions
   // if(user.address != null && points > 1000) { ... }

   // Premium user must have at least 1,000 points
   const PREMIUM_USER_REQUIREMENT_POINTS = 1000;

   // Permanent user must be registered address
   const isPermanentUser = user.address != null

   // "Premium User" is "Permanent User" and who has least 1,000 points
   const isPU = isPermanentUser && userPoints >= PREMIUM_USER_REQUIREMENT_POINTS;

   // Avoid meaningless code
   //if(isPU) {
   //  return true;
   //} else {
   //  return false;
   //}
   return isPU;
}

function setPremiumUser(userId) {
   // Check parameters ASAP. if result is NG, return ASAP.
   if(!userId) {
     return false;
   }

   // Always avoid reassignment (We don't need `var` or `let` except loop block or ISML)
   const user = getUserInfoById(userId);

   // NG
   // const user;
   // account = getUserInfoById(user);

   // Return ASAP in order to avoid a huge `if` block
   if(!user) {
     logger.warn('user not found: userId=' + userId)
     return;
   }
   // Avoid huge `if` block
   //if(user) {
   //   // Huge `if` block which has a lot of lines
   //   // ...
   //}

   // Avoid useless `else` block
   // else {
   //   const userPoints = getUserPointsByUserId(user.id);
   //   if(userPoints == null) {
   //   //...
   //}
   const userPoints = getUserPointsByUserId(user.id);
   if(userPoints == null) {
   	logger.warn('failed to fetch user points: userId' + userId)
     return;
   }

   // Avoid side effects as much as possible (isPremiumUser() shold not have any side effects)
   // function isPremiumUser(user, userPoints) {
   //   //...
   //    user.isPremiumUser = isPU;
   // }
   // isPremiumUser(user, userPoints);
   user.isPremiumUser = isPremiumUser(user, userPoints);
}
```

#### Do not use deprecated syntax

##### for each...in

```javascript
// BAD
for each (variable in object) {
  statement
}
```

- See also [https://developer.mozilla.org/en-US/docs/Web/JavaScript/Reference/Statements/for_each...in](https://developer.mozilla.org/en-US/docs/Web/JavaScript/Reference/Statements/for_each...in)

#### Avoid `!` conditions

```javascript
// Avoid `!` conditions
//if(!isSuccess) {
//	ng();
//} else {
//	ok();
//}
if(isSuccess) {
    ok();
} else {
    ng();
}
```

### Avoid `const` inside a loop block

- **SFCC `const` is limited support**

```javascript
while(orders.hasNext()) {
    // You cannot use `const` inside a loop block
    //const order = orders.next();
    var order = orders.next();
    //...
}
```

### Avoid `for` `while` loop

- Stop to use `for` `while` as much as possible
- You can use `while` only for the iterator of order search result or product search result or something like that
- Use `map` `reduce` `some` `every`... instead of `while` `for`

```javascript
const a = [1, 2, 3, 4, 5];

// NG
const b = [];
for(var i = 0; i < a.length; i++) {
  b[i] = 'No.' + a[i];
}

// OK
const c = a.map(function(i) {
  return 'No.' + i;
});
```

#### Avoid nesed blocks

```javascript
if(!isA) {
    if(isB) {
        foo();
    }
}
for(var i = 0; i < 10; i++) {
    for(var j = 0; j < 5; j++) {
        bar(i, j);
    }
}
```

#### Avoid messy unreadable conditions

```javascript
if(((a && a < 10) || (b && b.foo && b.foo > 10)) && (a && c && a > c))
```

#### Readability first

- We don't need a loop to process a fixed small amount of targets
  - **Readability first**

```javascript
// NG
const targets = ['foo', 'bar', 'baz'];
for(var i = 0; i < targets.length; i++) {
  doSomething(targets[i]);
}

// OK
doSomething('foo');
doSomething('bar');
doSomething('baz');
// or
// ['foo', 'bar', 'baz'].map(doSomething);
```

#### Use `One => One` function

- `One => One` function is better than `Array => Array` function
  - `One => One` function is easy to make a unit test

```javascript
// NG
 const convertedProducts1 = productsConverter(products);

 // OK
 const convertedProducts2 = products.map(productConverter);
```

#### Avoid reassigning

```javascript
// BAD
// It's hard to see what value you get in certain cases.
let foo1 = 'default';
if (condition) {
    foo1 = 'value';
    if (anotherCondition) {
    foo1 = 'another value';
    }
} else if (yetAnotherCondition) {
    foo1 = 'yet another value';
}
if (conditionInPriority) {
    foo1 = 'prior value'
}

// GOOD
// It's very clear and guaranteed that foo2 won't be altered after initialization.
const foo2 = (function(){
    if (conditionInPriority) {
        return 'prior value';
    }
    if (condition && moreCondition) {
        return 'more another';
    }
    if (condition) {
        return 'value';
    }
    if (yetAnotherCondition) {
        return 'yet another value';
    }
    return 'default';
})();

// Applicable to try ~ catch too
const foo3 = (function(){
    try {
        return mayThrowButYouJustWantToGetDafaultValueOnError();
    } catch(e) {
        return 'default';
    }
})();
```

#### Do not depend on values other than arguments

```javascript
// Do calculation with function parameters only.

// BAD: Only applicable for URLParameter although the calculation process doesn't depend on the source of the value.
function calculateBazWithURLParams() {
    const foobar = request.httpParameterMap.get('foobar').value;
    // some long calculation process ...
    return /baz/.test(foobar);
}

// GOOD: You can reuse this function in various cases.
function calculateBaz(foobar) {
    // some long calculation process ...
    return /baz/.test(foobar);
}

// OR: If the function will be called with the same value-source in ALMOST ALL CASES.
function calculateBazWithDefaultParameter(_foobar){
    const foobar = _foobar || request.httpParameterMap.get('foobar').value;
    // some long calculation process ...
    return /baz/.test(foobar);
}
```

#### Use `Calendar` in server side JS to avoid the lack of consideration for TimeZone

- Use `Calendar` instead of `Date` in server side JS

```javascript
// BAD calculated by GMT
function isOrderCreatedToday(order) {
    const orderDate = order.creationDate;
    const now = new Date();
    return orderDate.getDate() === now.getDate() &&
        orderDate.getMonth() === now.getMonth() &&
        orderDate.getFullYear() === now.getFullYear();
}

// GOOD concern about timezone
function isOrderCreatedTodayInTimezone(order) {
    const orderDate = order.creationDate;

    //NOTE: the following process can be extracted as a common function
    const orderCalendar = new dw.util.Calendar(orderDate );
    orderCalendar.setTimezone(dw.system.Site.current.timezone);

    const today = dw.system.Site.calendar;

    return orderCalendar.isSameDay(today);
}
```

- If you get a `Date` in server side, convert it to a `Calendar` immediately via [`withTimezoneFromDate()`](https://bitbucket.org/shiseidogroup/shiseidogroupapaccore/src/e9e03d1d6a672e72eeee1ab7371c27d23842f6f4/app_shiseidogroup_apaccore_jp/cartridge/scripts/util/CalendarUtils.js#lines-57) function
  - See also: https://bitbucket.org/wiredbeans/shiseido/pull-requests/4/add-new-guideline/diff#comment-116404895


#### Do not use `product` of `productLineItem` directly

- `productLineItem.product.foo` (without `null` check) often causes errors
- Use `ProductMgr.getProduct(productLineItem.productID)` instead
  - We can omit `null` check

```javascript
// BAD
const product = productLineItem.product;

// GOOD
const product = ProductMgr.getProduct(productLineItem.productID);
```


#### Use `*` for a `require` path

##### NOT Job script

- **Use `*`**

```javascript
const ProductUtils = require('*/cartridge/scripts/product/ProductUtils');
```

- **Stop to use config.js**
  - Use `*` instead of config.js

- If you want to require a parent module, use configs.js

```javascript
const Configs = require('~/configs');
const ProductCore =
require(Configs.shiseidogroupControllers + '/cartridge/controllers/Product');
```

#### Use placeholder(`{n}`) for a query string

- In order to avoid a security incident like "SQL injection"

```javascript
// BAD
var query =
  "customerNo='" + customer.profile.customerNo + "'" +
  " AND status!=" + dw.order.Order.ORDER_STATUS_CREATED +
  " AND status!=" + dw.order.Order.ORDER_STATUS_FAILED +
  "creationDate desc";
var orders = OrderMgr.searchOrders(query, null);

// GOOD
var orders = OrderMgr.searchOrders(
  'customerNo={0} AND status!={1} AND status!={2}',
  'creationDate desc',
  customer.profile.customerNo,
  dw.order.Order.ORDER_STATUS_CREATED,
  dw.order.Order.ORDER_STATUS_FAILED
);
```

##### Job script

- **Use `Configs` instead of `*`**

### Validator

TODO https://scrapbox.io/wiredbeans-s-project/Validator_%E8%A8%AD%E8%A8%88#5d36d9eb4652cd0000b9627e

### Function templates

TODO 関数のテンプレ定義する

## ISML

### Avoid `const` and `let` in ISLML

- Avoid to use `const` and `let` in ISLML in order to reassignment error in inherited ISML (Related ISML files are sharing their top level scope)

```html
<isscript>
  // const Configs = require('~/configs');
  var Configs = require('~/configs');
</isscript>
```

### Always use `encoding="on"`

- Always use `encoding="on"` to avoid XSS
- If you REALLY need `encoding="off"`, add `<iscomment>` for the reason

```html
<isprint value="${message}" encoding="on" />

<iscomment>`encoding=on` is default</iscomment>
<isprint value="${message}" />
```

- If you REALLY need `encoding="off"`, add `<iscomment>` for the reason

```html
<iscomment>BMで設定される値 (productDescription) なので encoding="off" で問題なし</iscomment>
<isprint value="${productDescription}" encoding="off" />
```

### Move messy logics to a model

```html
<iscomment>
    <isif condition="${foo && (foo.val > 0 || foo.name != 'bar' || foo.id != null }">
</iscomment>

<isif condition="${FooHelper.isValid(foo)}">
```

## CustomAttribute

About "SystemObject attributes", "CustomObject"

### Do not add new CustomAttirbute without a PR which updates a EnvChecker job

- If you add new CustomAttirbute
- Update a EnvChecker job to check your new CustomAttirbute
  - Add your new CustomAttribute to [a proper file](https://bitbucket.org/shiseidogroup/shiseidogroupapaccore/pull-requests/273/wb_gs-511/diff#chg-app_shiseidogroup_apaccore_jp/cartridge/configs/shiseido_global_jp.js)

Sample definition file

```javascript
// 設定ファイル名は対象のサイトのIDと一致している必要あり (nars_jp, shiseido_global_jp, etc）
// Sandbox環境の場合はジョブのカスタムパラメータ（siteID）に呼び出したいファイルのサイトIDを指定する

/**
 * 環境設定確認ジョブ用
 */
const EnvironmentValidator = {
  SystemObject: {
    SitePreferences: {
      system: ['lastModified', 'creationDate'],
      custom: ['gmo_shopID', 'gmo_shopPass', 'gmo_siteID', 'gmo_sitePass']
    }
  },
  CustomObject: {
    GmoPayType: {
      length: 3,
      system: ['lastModified', 'creationDate'],
      custom: ['ID', 'code', 'name']
    }
  }
};
module.exports = {
  EnvironmentValidator: EnvironmentValidator
};
```

### Fill out `description` and `help-test`

- Do not ommit to fill out `description` and `help-test`
- Describe [5W1H](https://en.wikipedia.org/wiki/Five_Ws) explicitly

### Stop processing if failed to get CustomAttributes

- 設定値が確実に存在する前提でプログラミング設計しない
  - 本番環境への設定反映処理は手動作業
  - 設定がなんらかのミスで消えることはありえる
- 設定の取得に失敗した場合は、処理を継続せずにエラー終了にする
  - 例外を投げる
  - Fatal レベルでログを出力する (Fatal レベルでロギンングするとメールが飛ぶので)
- **決済処理等の極めて重大な設定値は、取得に失敗した状態で先に進めないことをコードレベルで保証する**


### CustomAttributes for TEST

- 原則としてテスト用設定は使用しない
  - Mock 等でやりきる
- どうしても必要な場合のみ追加する
  - ID には `test` prefix を付ける e.g. `testFooBarName`
  - 追加した設定は「W+ Mock 用属性グループ」もしくは「Test 属性グループ」のいずれかに追加する
  - 設定値は Production、および Staging では原則オフ(ないし空)
  - 設定が必要なときだけ一時的に設定する
    - テスト用設定が Production で使用されないようにコードレベルで保証する
    - 参考: [https://bitbucket.org/shiseidogroup/shiseidogroupapaccore/pull-requests/181/line-id/diff](https://bitbucket.org/shiseidogroup/shiseidogroupapaccore/pull-requests/181/line-id/diff)

## Client side JavaScript

### Use a `js-` prefix if it is only used by JavaScript

```
<button class="js-foo-button"></button>
```

- If the CSS class name is used by JavaScript only (no CSS styles for the class), use a `js-` prefix

### Event listener

- Event `off` first
  - To avoid registering duplicate event listeners

```javascript
// BAD
$('hoge').on('click', selector, callback);

// GOOD
$('hoge').off('click.[固有のID]', selector).on('click.[固有のID]', selector, callback);
```

- Even better if you use `namespace`

```javascript
$('.class').bind('click.namespace', function(){}); 
$('.class').trigger('click.namespace'); // namespace名前空間だけ
$('.class').trigger('click');           // 全ての関数
$('.class').trigger('click.other');     // otherだけ
```

- 参考: [http://semooh.jp/jquery/cont/doc/namespaced_event/](http://semooh.jp/jquery/cont/doc/namespaced_event/)